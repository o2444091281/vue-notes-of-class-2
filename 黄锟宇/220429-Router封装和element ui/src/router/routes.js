
import Navigation from '../components/pages/NavigatioN'
import ContenT from '../components/pages/ContenT'
import ToP from '../components/pages/ToP'
import MaiN from '../components/pages/MaiN'

let routes=[//注意是routes不是routers
{
    path:'/',
    component:ContenT,
    children:[
        {
            path:'',
            components:{
                Navigation,
                ToP,
                MaiN
            },
        },

        ]
    }
]
export default routes