import VueRouter from 'vue-router'//引入模块
import Vue from 'vue'//引入模块
import routes from './routes'//引入模块

Vue.use(VueRouter)//1111

export default new VueRouter({//创建路由管理
    routes,
    mode:'history'
})